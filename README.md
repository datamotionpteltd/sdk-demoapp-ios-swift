# Raxel Demo App Swift #

This README would normally document whatever steps are necessary to get your application up and running.


This is an app that demostrate using of RaxelPulse framework. The app will track the person's driving behavior such as speeding, turning, braking and several other things.

### Installation (Mac) ###
To run the demo app:

* clone this repository to local folder
* run pod install from project's folder.
* open AppDelegate.m and insert your Device Token to this:
* RPEntry.instance().virtualDeviceToken = "<some virtual device token>";
* implement code signing if you want to install this app to your iOS device
